const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

const port = 8000;

app.use(express.static(__dirname + "/views"))


// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/pizza365index.html"))
    response.sendFile(path.join(__dirname + "/views/css"))
    response.sendFile(path.join(__dirname + "/views/images"))
})
app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})